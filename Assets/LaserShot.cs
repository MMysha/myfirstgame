﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserShot : MonoBehaviour
{
    public float speed;
    public GameObject playerExplosion;
    public GameObject asteroidExplosion;

    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.tag == "RightShot")
        {
            GetComponent<Rigidbody>().velocity = new Vector3(1, 0, 1) * speed;
        }
        else if (gameObject.tag == "LeftShot")
        {
            GetComponent<Rigidbody>().velocity = new Vector3(-1, 0, 1) * speed;
        }
        else if (gameObject.tag == "EnemyShot")
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, -1) * speed;
        }
        else {
            GetComponent<Rigidbody>().velocity = Vector3.forward * speed;
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "GameBoundary")
        {
            return;
        }
        
        if (other.tag == "Player" && (gameObject.tag == "ForwardShot" || gameObject.tag == "LeftShot" || gameObject.tag == "RightShot"))
        {
            return;
        }

        if (gameObject.tag == "EnemyShot" && other.tag == "Player")
        {
            Instantiate(playerExplosion, other.transform.position, Quaternion.identity);
        }

        if (gameObject.tag == "EnemyShot" && other.tag == "Asteroid")
        {
            Instantiate(asteroidExplosion, other.transform.position, Quaternion.identity);
        }

        Destroy(gameObject);
        Destroy(other.gameObject);
    }
    
}