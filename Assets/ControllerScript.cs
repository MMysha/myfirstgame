﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerScript : MonoBehaviour
{
    public GameObject scoreText;

    private int score = 0;
    private UnityEngine.UI.Text textComponent;

    public void Start()
    {
        textComponent = scoreText.GetComponent<UnityEngine.UI.Text>();
    }

    public void updateScore(int scoreIncrement)
    {
        score += scoreIncrement;
        textComponent.text = "Score: " + score;
    }
}
