﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public GameObject laserShot;    // любой игровой объект можно положить сюда, кладем в среде Unity

    public Transform laserGun;      // нас не интересует сам объект, только его позиция, поворот
    public Transform leftGun, rightGun;
    public float delay;             // как часто можно стрелять

    private float nextShot;         // время, когда уже пора стрелять
    private float nextShotSmall;

    Rigidbody player;
    public float speed; // эту переменную можно менять из настроек Unity
    public float tilt;

    public float xMin, xMax, zMin, zMax; // значение задаем в Unity

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float moveHorizontal = Input.GetAxis("Horizontal"); // Влево или вправо ?   -1 ..... +1
        float moveVertacal = Input.GetAxis("Vertical"); 

        player.velocity = new Vector3(moveHorizontal, 0, moveVertacal) * speed;

        player.rotation = Quaternion.Euler(player.velocity.z * tilt, 0, - player.velocity.x * tilt);

        float positionX = Mathf.Clamp(player.position.x, xMin, xMax);
	    float positionZ = Mathf.Clamp(player.position.z, zMin, zMax);
	
	    player.position = new Vector3(positionX, 0, positionZ); // движение ограничено

        if (Input.GetButton("Fire1") && Time.time > nextShot) 
        { 
            GameObject shot = Instantiate(laserShot, laserGun.position, Quaternion.identity); // что создаем, где создаем, пустой поворот
            shot.gameObject.tag = "ForwardShot";
            nextShot = Time.time + delay;
        }

        if (Input.GetButton("Fire2") && Time.time > nextShotSmall)
        {
            GameObject shot1 = Instantiate(laserShot, leftGun.position, Quaternion.identity);
            GameObject shot2 = Instantiate(laserShot, rightGun.position, Quaternion.identity);

            shot1.gameObject.tag = "LeftShot";
            shot1.transform.localScale = new Vector3(1, 1, 1);
            shot1.transform.rotation = Quaternion.Euler(0, -45, 0);

            shot2.gameObject.tag = "RightShot";
            shot2.transform.localScale = new Vector3(1, 1, 1);
            shot2.transform.rotation = Quaternion.Euler(0, 45, 0);
            
            nextShotSmall = Time.time + delay/2;
        }
    }
}
