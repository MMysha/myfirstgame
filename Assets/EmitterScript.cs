﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmitterScript : MonoBehaviour
{
    public GameObject asteroid, asteroid2, asteroid3;
    public GameObject enemy;

    public float minDelay, maxDelay;
    public float nextLaunch;

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextLaunch)
        {
            nextLaunch = Time.time + Random.Range(minDelay, maxDelay);
            float positionX = Random.Range(-transform.localScale.x / 2, transform.localScale.x / 2);
            Vector3 newPosition = new Vector3(positionX, transform.position.y, transform.position.z);

            int type = Random.Range(0, 4);

            if (type == 0)
            {
                Instantiate(asteroid, newPosition, Quaternion.identity);
            }
            else if (type == 1)
            {
                Instantiate(asteroid2, newPosition, Quaternion.identity);
            }
            else if (type == 2)
            {
                Instantiate(asteroid3, newPosition, Quaternion.identity);
            }
            else if (type == 3)
            {
                Instantiate(enemy, newPosition, Quaternion.identity);
            }
            
        }
    }
}
