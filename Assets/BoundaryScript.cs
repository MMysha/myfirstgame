﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundaryScript : MonoBehaviour
{
    // Метод - срабатывает при выходе другого объекта other из границ текущего коллайдера
    void OnTriggerExit(Collider other)
    {
        Destroy(other.gameObject);
    }
}
