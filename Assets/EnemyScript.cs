﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    public float minSpeed, maxSpeed;

    public GameObject playerExplosion;
    public GameObject asteroidExplosion;

    public GameObject laserShot;

    public Transform laserGun;
    private float nextShot;
    private float delay = 1;

    // Start is called before the first frame update
    void Start()
    {
        Rigidbody enemy = GetComponent<Rigidbody>();
        enemy.transform.rotation = Quaternion.Euler(0, 180, 0);
        enemy.velocity = Vector3.back * Random.Range(minSpeed, maxSpeed);
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextShot)
        {
            GameObject shot = Instantiate(laserShot, laserGun.position, Quaternion.identity);
            shot.gameObject.tag = "EnemyShot";
            shot.transform.localScale = new Vector3(0.5f, 0, 0.5f);

            nextShot = Time.time + delay;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "GameBoundary")
        {
            return;
        }

        Instantiate(playerExplosion, transform.position, Quaternion.identity);

        if (other.tag == "Player")
        {
            Instantiate(playerExplosion, other.transform.position, Quaternion.identity);
        }

        if (other.tag == "Asteroid")
        {
            Instantiate(asteroidExplosion, other.transform.position, Quaternion.identity);
        }

        Destroy(gameObject);
        Destroy(other.gameObject);
    }
}
