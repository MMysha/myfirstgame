﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidScript : MonoBehaviour
{
    public GameObject playerExplosion;
    public GameObject asteroidExplosion;

    public float rotation;
    public float minSpeed, maxSpeed;

    private ControllerScript gameController;

    // Start is called before the first frame update
    void Start()
    {
        Rigidbody asteroid = GetComponent<Rigidbody>();
        asteroid.angularVelocity = Random.insideUnitSphere * rotation; // cлучайный вектор вращения
        asteroid.velocity = Vector3.back * Random.Range(minSpeed, maxSpeed); // астероид будет лететь вниз

        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<ControllerScript>();
    }

    // вызовется, когда другой коллайдер столкнется с текущим
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Asteroid" || other.tag == "GameBoundary")
        {
            return;
        }

        Instantiate(asteroidExplosion, transform.position, Quaternion.identity); // transform.position - текущее местоположение объекта

        if (other.tag == "Player" || other.tag == "Enemy")
        {
            Instantiate(playerExplosion, other.transform.position, Quaternion.identity);
        } else
        {
            gameController.updateScore(10);
        }

        // уничтожить выстрел и астероид
        Destroy(gameObject); // уничтожаем астероид
        Destroy(other.gameObject); // уничтожаем второй объект
    }
}
